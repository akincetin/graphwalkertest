## How to Use 

This is a test that runs on the Ciceksepeti web site.

Run the test using maven:

```bash
$ mvn clean
$ mvn graphwalker:generate-sources
$ mvn graphwalker:test
$ mvn graphwalker:validate-models
$ mvn graphwalker:validate-test-models
```
## Test Scenarios

### Scenario 1

In the first scenario the application goes to cicepsepeti.com and goes for a simple search
after competing the search the scenario continues with going into details of a certain product
after covering every edge, scenario can be considered successful. In every vertex the software
validates if the page is the correct page.


### Scenario 2

This scenario focuses on cart. After going to ciceksepeti base url software goes to cart and comes back to
home url. After covering every edge, scenario can be considered as successful. In every vertex the software
validates if the page is the correct page.


### Scenario 3

This scenario focuses on filtered products. After going to cicepsepeti base url test continues with going to
the cicek filter and after completing that step, it clicks on dogum gunu cicekleri for further search. The scenario
can be considered as successful, after covering every edge. In every vertex the software validates if the page is the correct page.
In every vertex the software validates if the page is the correct page.


### Scenario 4

This scenario focuses on order tracking. After going to ciceksepeti home url, software continues to order tracking page and
returns to the home url. The scenario can be considered as successful after covering every edge. In every vertex the software
validates if the page is the correct page.


### Discussion

Because of the nature of the website, using ```findElement(By.xpath)``` may create problems with the tests. There is a pop up window when we first
go to the website and if the scenario tries to go to that specific xpath after that pop up opens, the software cannot find the
desired path and the test fails but if the random edge goes to the path before pop up window opens, the test does not fail.
This situation should be taken into account when creating test cases.