// Generated by GraphWalker (http://www.graphwalker.org)
package com.mbtroads;

import org.graphwalker.java.annotation.Model;
import org.graphwalker.java.annotation.Vertex;
import org.graphwalker.java.annotation.Edge;

@Model(file = "com/mbtroads/CicekSepeti.json")
public interface CicekSepeti4 {

    @Vertex()
    void v_OrderTrackingPage();

    @Edge()
    void e_ToOrderTracking();

    @Vertex()
    void v_CicekSepeti();

    @Edge()
    void e_HomeURL();
}
