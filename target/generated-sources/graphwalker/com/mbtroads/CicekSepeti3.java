// Generated by GraphWalker (http://www.graphwalker.org)
package com.mbtroads;

import org.graphwalker.java.annotation.Model;
import org.graphwalker.java.annotation.Vertex;
import org.graphwalker.java.annotation.Edge;

@Model(file = "com/mbtroads/CicekSepeti.json")
public interface CicekSepeti3 {

    @Edge()
    void e_ToFiltered();

    @Vertex()
    void v_FilteredProducts();

    @Vertex()
    void v_ItemDetails();

    @Edge()
    void e_ClickItem();

    @Vertex()
    void v_CicekSepeti();

    @Edge()
    void e_HomeURL();
}
