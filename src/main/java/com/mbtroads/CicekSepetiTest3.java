package com.mbtroads;

import org.graphwalker.core.machine.ExecutionContext;
import org.graphwalker.java.annotation.GraphWalker;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.Assert;

import java.time.Duration;


/**
 * This test implements CicekSepeti3 and it opens a chrome browser goes to cicepsepeti homepage and goes to filtered products
 * and goes into an item's details
 */
@GraphWalker(value="random(edge_coverage(100))")
public class CicekSepetiTest3 extends ExecutionContext implements CicekSepeti3 {
  WebDriver driver = new ChromeDriver();
  WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds( 10));

  @Override
  public void e_ToFiltered() {

    driver.get("https://www.ciceksepeti.com/cicek");


  }

  @Override
  public void v_FilteredProducts() {
    String URL = driver.getCurrentUrl();
    Assert.assertEquals(URL, "https://www.ciceksepeti.com/cicek" );

  }

  @Override
  public void v_ItemDetails() {
    String URL = driver.getCurrentUrl();
    Assert.assertEquals(URL, "https://www.ciceksepeti.com/dogum-gunu-cicekleri" );
  }

  @Override
  public void e_ClickItem() {
    driver.get("https://www.ciceksepeti.com/dogum-gunu-cicekleri");


  }

  @Override
  public void v_CicekSepeti() {
    String URL = driver.getCurrentUrl();
    if (URL.equals("data:,")){
      return;

    }else{
      Assert.assertEquals(URL, "https://www.ciceksepeti.com/" );
    }


  }

  @Override
  public void e_HomeURL() {
    driver.get("https://www.ciceksepeti.com/");
    //driver.findElement(By.name("q")).sendKeys("cheese" + Keys.ENTER);

  }
}
