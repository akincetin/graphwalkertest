package com.mbtroads;

import org.graphwalker.core.machine.ExecutionContext;
import org.graphwalker.java.annotation.GraphWalker;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.Assert;

import java.time.Duration;
/**
 * This test implements CicekSepeti4, it opens a chrome browser and goes to ciceksepeti's homepage and after that it goes
 * to order tracking page
 */
@GraphWalker(value="random(edge_coverage(100))")
public class CicekSepetiTest4 extends ExecutionContext implements CicekSepeti4 {
  WebDriver driver = new ChromeDriver();
  WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds( 10));


  @Override
  public void v_OrderTrackingPage() {
    String URL = driver.getCurrentUrl();
    Assert.assertEquals(URL, "https://www.ciceksepeti.com/siparis-takip" );


  }

  @Override
  public void e_ToOrderTracking() {

    driver.get("https://www.ciceksepeti.com/siparis-takip");


  }

  @Override
  public void v_CicekSepeti() {
    String URL = driver.getCurrentUrl();
    if (URL.equals("data:,")){
      return;

    }else{
      Assert.assertEquals(URL, "https://www.ciceksepeti.com/" );
    }


  }

  @Override
  public void e_HomeURL() {
    driver.get("https://www.ciceksepeti.com/");
    //driver.findElement(By.name("q")).sendKeys("cheese" + Keys.ENTER);

  }
}
