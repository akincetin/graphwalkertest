package com.mbtroads;

import org.graphwalker.core.machine.ExecutionContext;
import org.graphwalker.java.annotation.GraphWalker;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.Assert;

import java.time.Duration;

/**
 * This test implements CicekSepeti1 and this test scenario is testing main models of ciceksepeti. It opens a chrome driver
 * goes to ciceksepeti.com, searches for a product and goes into product's details
 */
@GraphWalker(value="random(edge_coverage(100))")
public class CicekSepetiTest1 extends ExecutionContext implements CicekSepeti1 {

  WebDriver driver = new ChromeDriver();
  WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds( 10));



  @Override
  public void v_SearchResults() {
    String URL = driver.getCurrentUrl();
    Assert.assertEquals(URL, "https://www.ciceksepeti.com/Arama?query=beyaz%20papatya&qt=beyaz%20papatya" );

  }

  @Override
  public void v_ItemDetails() {
    String URL = driver.getCurrentUrl();
    Assert.assertEquals(URL, "https://www.ciceksepeti.com/beyaz-papatyalar-cicek-buketi-at4253");
  }

  @Override
  public void e_ClickItem() {
    driver.findElement(By.xpath("//a[@href='/beyaz-papatyalar-cicek-buketi-at4253']")).click();

  }

  @Override
  public void e_SearchItem() {

    //WebElement search = driver.findElement(By.id("product-search-2"));
    //search.sendKeys("papatya");
    //search.click();
    //driver.findElement(By.id("product-search-2")).sendKeys("beyaz papatya" + Keys.ENTER);
    driver.get("https://www.ciceksepeti.com/Arama?query=beyaz%20papatya&qt=beyaz%20papatya");
  }

  @Override
  public void v_CicekSepeti() {
    String URL = driver.getCurrentUrl();
    Assert.assertEquals(URL, "https://www.ciceksepeti.com/" );

  }

  @Override
  public void e_HomeURL() {

      driver.get("https://www.ciceksepeti.com/");
      //driver.findElement(By.name("q")).sendKeys("cheese" + Keys.ENTER);

  }
}
