package com.mbtroads;

import org.graphwalker.core.machine.ExecutionContext;
import org.graphwalker.java.annotation.GraphWalker;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.Assert;

import java.time.Duration;

/**
 * This test implements CicekSepeti2 and it opens a chrome browser checks the shopping cart and goes back
 */


@GraphWalker(value="random(edge_coverage(100))")
public class CicekSepetiTest2 extends ExecutionContext implements CicekSepeti2 {

  WebDriver driver = new ChromeDriver();
  WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds( 10));
  //System.setProperty("webdriver.chrome.driver", "/opt/WebDriver/bin/chromedriver");



  @Override
  public void v_SearchResults() {
    String URL = driver.getCurrentUrl();
    Assert.assertEquals(URL, "https://www.ciceksepeti.com/" );
  }

  @Override
  public void v_Chart() {
    String URL = driver.getCurrentUrl();
    Assert.assertEquals(URL, "https://www.ciceksepeti.com/sepetim");

  }

  @Override
  public void e_StartShopping() {
    driver.get("https://www.ciceksepeti.com/");

  }

  @Override
  public void e_ToChart() {
    driver.get("https://www.ciceksepeti.com/sepetim");

  }

  @Override
  public void v_CicekSepeti() {
    String URL = driver.getCurrentUrl();
    if (URL.equals("data:,")){
      return;

    }else{
      Assert.assertEquals(URL, "https://www.ciceksepeti.com/" );
    }


  }

  @Override
  public void e_HomeURL() {
    driver.get("https://www.ciceksepeti.com/");
    //driver.findElement(By.name("q")).sendKeys("cheese" + Keys.ENTER);
  }
}
